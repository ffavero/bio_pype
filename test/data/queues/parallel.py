import os
import sys
import yaml
import datetime
import time
import shlex
import subprocess
from multiprocessing import Queue, Pool
from psutil import cpu_count, virtual_memory
from pype.utils.queues import yaml_dump
from pype.misc import bases_format
from pype.__config__ import PYPE_NCPU, PYPE_MEM

if PYPE_NCPU:
    MAX_CPU = int(PYPE_NCPU)
else:
    MAX_CPU = cpu_count()

if PYPE_MEM:
    MAX_MEM = bases_format(PYPE_MEM, 1024)
else:
    MAX_MEM = virtual_memory()[1]


def submit(command, snippet_name, requirements, dependencies, log, profile):
    run_id = yaml_dump(command, snippet_name, requirements,
                       dependencies, log, profile)
    return(run_id)


def post_run(log):
    log.add_log('parallel_run')
    queuelog = log.programs_logs['parallel_run']
    root_dir = log.__path__
    queuelog.log.info('Creating yaml_db folder in %s' % root_dir)
    db_dir = os.path.join(queuelog.__path__, 'yaml_db')
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)
    yaml_file = os.path.join(root_dir, 'pipeline_runtime.yaml')
    if os.path.isfile(yaml_file):
        with open(yaml_file, 'rt') as pipeline_runtime:
            runtime = yaml.safe_load(pipeline_runtime)
    else:
        runtime = dict()
    tasks = list(runtime.keys())
    tasks_running = list()

    queue = Queue()
    queuelog.log.info('Start queue pool of %i cpu(s)' % MAX_CPU)
    pool = Pool(MAX_CPU, mp_worker, (queue,))

    init_tasks = len(tasks)
    task_index = 0
    used_cpus = 0
    used_mem = 0
    while len(tasks) > 0:
        task = tasks[task_index]
        task_data = runtime[task]
        task_status = record_read(task, db_dir)
        try:
            task_cpus = int(task_data['requirements']['ncpu'])
        except KeyError:
            task_cpus = 1
        try:
            task_mem = bases_format(task_data['requirements']['mem'], 1024)
        except KeyError:
            task_mem = 1024 ** 3
        if task_status is None:
            run_task = True
            for dep in task_data['dependencies']:
                if dep in tasks:
                    run_task = False
            if run_task is True:
                if task_cpus + used_cpus > MAX_CPU:
                    run_task = False
                if task_mem + used_mem > MAX_MEM:
                    if task_mem > MAX_MEM:
                        queuelog.log.error(('Not enough memory for the job: '
                                            '%i needed, %i total, %1.f%%') %
                                           (task_mem, MAX_MEM,
                                            float(task_mem) / MAX_MEM * 100))
                        break
                    run_task = False
            if run_task:
                if task in tasks_running:
                    queuelog.log.warning('Job %s should be already running' %
                                         task)
                else:
                    used_cpus += task_cpus
                    used_mem += task_mem
                    tasks_running.append(task)
                    queue.put((task, db_dir, task_data))
        else:
            if 'end' in task_status.keys():
                used_cpus -= task_cpus
                used_mem -= task_mem
                tasks.remove(task)
                tasks_running.remove(task)
            else:
                pass

        if task_index >= (len(tasks) - 1):
            task_index = 0
        else:
            task_index += 1
        q_msg = ('%i job(s) running, %i jobs(s) left, '
                 '%i cpu(s) of %i used, %.1f%% memory used') % (
                    len(tasks_running), len(tasks), used_cpus,
                    MAX_CPU, float(used_mem) / MAX_MEM * 100)
        progress(init_tasks - len(tasks), init_tasks, q_msg, 20)

        time.sleep(1)
    pool.close()
    pool.terminate()
    queuelog.log.info('Parallel run terminated %i job(s)' % len(tasks))


def mp_worker(queue):
    while True:
        task, db_dir, data = queue.get(True)
        record_start(task, db_dir)
        command = shlex.split(data['command'])
        log_index = command.index('--log')
        path = command[log_index + 1]
        stdout = os.path.join(path, 'stdout')
        stderr = os.path.join(path, 'stderr')
        with open(stdout, 'wt') as std_out, open(stderr, 'wt') as std_err:
            command_proc = subprocess.Popen(command,  stdout=std_out,
                                            stderr=std_err)
            command_proc.communicate()[0]
        time.sleep(1)
        record_end(task, db_dir)


def record_read(task, db_dir):
    yaml_file = os.path.join(db_dir, '%s_db.yaml' % task)
    if os.path.isfile(yaml_file):
        with open(yaml_file, 'rt') as task_file:
            record = yaml.safe_load(task_file)
        return record
    else:
        return None


def record_start(task, db_dir):
    start_time = datetime.datetime.now()
    record = dict()
    record['start'] = start_time
    yaml_file = os.path.join(db_dir, '%s_db.yaml' % task)
    if not os.path.isfile(yaml_file):
        with open(yaml_file, 'wt') as task_file:
            yaml.dump(record, task_file,
                      default_flow_style=False)
    else:
        raise Exception('File %s already exists' % yaml_file)


def record_end(task, db_dir):
    end_time = datetime.datetime.now()
    yaml_file = os.path.join(db_dir, '%s_db.yaml' % task)
    if os.path.isfile(yaml_file):
        with open(yaml_file, 'rt') as task_file:
            record = yaml.safe_load(task_file)
    else:
        raise Exception('File %s does not exists' % yaml_file)
    record['end'] = end_time
    with open(yaml_file, 'wt') as task_file:
        yaml.dump(record, task_file,
                  default_flow_style=False)


def progress(count, total, status='', bar_len=60):
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    if percents < 100:
        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
        sys.stdout.flush()
    else:
        sys.stdout.write('[%s] %s%s ...%s\n' % (bar, percents, '%', status))
