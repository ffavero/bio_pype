from pype.misc import xopen


def requirements():
    return({'ncpu': 1, 'time': '00:01:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'out': file})


def friendly_name(argv):
    try:
        words = argv['--word']
    except KeyError:
        words = argv['-w']
    if isinstance(words, list):
        words = words.split(' ')
    word = '_'.join([w.replace('/', '_') for w in words])

    return('hello_%s' % word)


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='hello world with flag', add_help=False)


def hello_args(parser, argv):
    parser.add_argument('-w', '--word', dest='word',
                        help='word to print', required=True)
    parser.add_argument('-o', '--output', dest='output',
                        help='Output file', required=True)
    parser.add_argument('--hello', dest='hello',
                        help='Say hello or not', action="store_true")
    parser.add_argument('--no-upper', dest='no_upper',
                        help='Do not upper case', action="store_false")
    return parser.parse_args(argv)


def hello(parsers, module_name, argv, profile, log):
    log.log.info('Processing arguments')
    log.log.info(argv)
    message = list()
    args = hello_args(add_parser(
        parsers, module_name), argv)
    log.log.info('Open output file %s' % args.output)
    with xopen(args.output, 'wt') as output:
        log.log.info('Reading the word to print')
        if args.hello:
            message.append('hello')
        message.append(args.word)

        log.log.info('writing results into %s' % args.output)
        if args.no_upper:
            message = [m.upper() for m in message]
        else:
            message = [m.lower() for m in message]
        output.write('%s\n' % ' '.join(message))
    log.log.info('Closing input/output files')
