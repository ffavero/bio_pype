import os


def requirements():
    return({'ncpu': 1, 'time': '00:01:00'})


def results(argv):
    try:
        file = argv['--output']
    except KeyError:
        file = argv['-o']
    return({'out': file})


def friendly_name(argv):
    try:
        input_file = argv['--input']
    except KeyError:
        input_file = argv['-i']
    input_file = input_file.replace('.gz', '')
    input_file = input_file.replace('.txt', '')

    return('reverse_fa_%s' % os.path.basename(input_file))


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='reverse a fasta sequence', add_help=False)


def reverse_fa_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input fasta file', required=True)
    parser.add_argument('-o', '--output', dest='output',
                        help='Input fasta file', required=True)
    return parser.parse_args(argv)


def reverse_fa(subparsers, module_name, argv, profile, log):
    args = reverse_fa_args(add_parser(
        subparsers, module_name), argv)
    with open(
        args.input, 'rt') as input_file, open(
            args.output, 'wt') as output:
        fasta_file = fasta(input_file)
        for item in fasta_file:
            output.write('>%s reverse\n' % item[0])
            line_len = len(item[1])
            while line_len > 61:
                output.write('%s\n' % item[1][line_len:line_len - 61:-1])
                line_len -= 61
            if line_len > 0:
                output.write('%s\n' % item[1][line_len::-1])


def fasta(file):
    header, sequence = '', ''
    for line in file:
        if line.startswith('>'):
            if sequence:
                yield (header, sequence)
            header = line[1:-1]
            sequence = ''
        else:
            sequence += line.strip()
    yield (header, sequence)
