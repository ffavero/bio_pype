def requirements():
    return({'ncpu': 1, 'time': '00:01:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'out': file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='lower case a fasta sequence', add_help=False)


def lower_fa_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input fasta file', required=True)
    parser.add_argument('-o', '--output', dest='output',
                        help='Input fasta file', required=True)
    return parser.parse_args(argv)


def lower_fa(subparsers, module_name, argv, profile, log):
    args = lower_fa_args(add_parser(
        subparsers, module_name), argv)
    with open(
        args.input, 'rt') as input_file, open(
            args.output, 'wt') as output:
        fasta_file = lower_fasta(input_file)
        for line in fasta_file:
            output.write('%s\n' % line)


def lower_fasta(file):
    for line in file:
        if line.startswith('>'):
            yield "%s lowered" % line.strip()
        else:
            yield line.strip().lower()
