def requirements():
    return({'ncpu': 1, 'time': '00:01:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'out': file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Concatenate a series of files into a single one',
        add_help=False)


def merge_fa_args(parser, argv):
    parser.add_argument('-l', '--list', dest='list', nargs='*',
                        help='Input fasta file', required=True)
    parser.add_argument('-o', '--output', dest='output',
                        help='Input fasta file', required=True)
    return parser.parse_args(argv)


def merge_fa(subparsers, module_name, argv, profile, log):
    args = merge_fa_args(add_parser(
        subparsers, module_name), argv)
    with open(args.output, 'wt') as output:
        for file in args.list:
            with open(file, 'rt') as fa_i:
                for line in fa_i:
                    output.write('%s\n' % line.strip())
