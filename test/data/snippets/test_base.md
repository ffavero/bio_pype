# Example test Snippet

The snippet in pype is given by the file name
(minus the `.md` extension)

## description

Test snippet example

## requirements

```yaml
ncpu: 1
time: '00:01:00'
mem: 1gb
```

## results

```bash
@/bin/sh, yaml


printf  'file_out: %(output)s'
```

## arguments

1. input/i

    - help: input(s) text file
    - type: str
    - required: true
    - nargs: *

2. output/o

    - help: output file
    - type: str
    - default: output.txt

## snippet

> _input_: input profile_dummy_file*

```bash
@/bin/sh, chk1, stdout=chk2, namespace=alpine_3

files_input='%(input)s'

dummy_file='%(profile_dummy_file)s'

cat $files_input $dummy_file | awk '{ print toupper($0) }'

```

> _output_: results_file_out

```bash
@/bin/sh, chk2, namespace=alpine_3

awk '{ print tolower($0) }' > '%(output)s'

```
