def requirements():
    return({'ncpu': 1, 'time': '00:01:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'out': file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='lower case a fasta sequence', add_help=False)


def complement_fa_args(parser, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input fasta file', required=True)
    parser.add_argument('-o', '--output', dest='output',
                        help='Input fasta file', required=True)
    return parser.parse_args(argv)


def complement_fa(subparsers, module_name, argv, profile, log):
    log.log.info('Processing arguments')
    log.log.info(argv)
    args = complement_fa_args(add_parser(
        subparsers, module_name), argv)
    log.log.info('Open input/output files %s and %s' %
                 (args.input, args.output))
    with open(
        args.input, 'rt') as input_fa, open(
           args.output, 'wt') as output:
        log.log.info('Transforming FASTA into complementary sequence')
        fasta_file = complement_fasta(input_fa)
        log.log.info('writing results into %s' % args.output)
        for line in fasta_file:
            output.write('%s\n' % line)
    log.log.info('Closing input/output files')


def complement_fasta(file):
    complement = {'A': 'T', 'C': 'G', 'G': 'C',
                  'T': 'A', 'a': 't', 'c': 'g', 'g': 'c', 't': 'a'}
    for line in file:
        if line.startswith('>'):
            yield "%s complement" % line.strip()
        else:
            yield ''.join([complement.get(c, c) for c in line.strip()])
