##
# cSpell:word pype

import unittest
import sys
import os
from pype import argparse
from pype.modules import snippets
from pype.misc import generate_uid
from io import StringIO



class TestSnippets(unittest.TestCase):

    def setUp(self):
        self.held, sys.stdout = sys.stdout, StringIO()
        self.parser = argparse.ArgumentParser(prog='pype', description='Test')
        self.subparsers = self.parser.add_subparsers(dest='modules')

    def test_snippets(self):
        a = generate_uid()
        b = generate_uid()
        c = generate_uid()

        tmp_path = os.path.join('test', 'data', 'tmp')
        input_a = os.path.join(tmp_path, '%s.txt' % a)
        output_b = os.path.join(tmp_path, '%s.txt' % b)
        output_c = os.path.join(tmp_path, '%s.txt' % c)
        with open(input_a, 'wt') as file_in:
            file_in.write(a)

        snippets.snippets(self.subparsers, None, [
            '--log', tmp_path, 'test_base', '--input', input_a,
            '--output', output_b], 'test_path')
        with open(output_b, 'rt') as file_out:
            self.assertEqual(
                file_out.read(), '%sthis is a dummy file!\n' % a.lower())
        snippets.snippets(self.subparsers, None, [
            '--log', tmp_path, 'test_adv', '--input', input_a,
            '--output', output_c], 'test_path')
        with open(
            output_b, 'rt') as file_out1, open(
                output_c, 'rt') as file_out2:
            self.assertEqual(
                file_out1.read(), file_out2.read())

    def test_friendly_name(self):
        friendly_name = snippets.PYPE_SNIPPETS_MODULES[
            'reverse_fa'].friendly_name({'--input': 'test_file.txt'})
        self.assertEqual(friendly_name, 'reverse_fa_test_file')

    def tearDown(self):
        sys.stdout = self.held


if __name__ == '__main__':
    unittest.main()
