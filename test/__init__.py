"""
Prepare the test data

Cleanup temporary folder and assign test
environment variables
"""

import os
from shutil import rmtree

test_dir = os.path.abspath(
    os.path.join('test', 'data'))
tmp_dir = os.path.join(test_dir, 'tmp')
try:
    rmtree(tmp_dir)
    os.mkdir(tmp_dir)
except OSError:
    os.mkdir(tmp_dir)

test_profiles_dir = os.path.join(tmp_dir, 'profiles')
test_snippets_dir = os.path.join(test_dir, 'snippets')
test_pipelines_dir = os.path.join(test_dir, 'pipelines')
test_queues_dir = os.path.join(test_dir, 'queues')

dummy_file_path = os.path.join(test_dir, 'files', 'dummy_file.txt')
default_profile_path = os.path.join(test_dir, 'profiles', 'test_path.yaml')
os.mkdir(os.path.join(test_profiles_dir))

# Adjust the profile to match the test data
with open(
    default_profile_path, 'rt') as default_profile, open(
        os.path.join(
            test_profiles_dir, 'test_path.yaml'), 'wt') as tmp_profile:
    profiles_data = default_profile.read()
    tmp_profile.write(profiles_data.replace(
        '/just/a/dummy/file/for/testing.txt', dummy_file_path))

os.environ['PYPE_PROFILES'] = test_profiles_dir
os.environ['PYPE_SNIPPETS'] = test_snippets_dir
os.environ['PYPE_PIPELINES'] = test_pipelines_dir
os.environ['PYPE_QUEUES'] = test_queues_dir
