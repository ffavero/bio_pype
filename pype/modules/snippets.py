"""Snippet command-line interface module.

This module provides the command-line interface for snippet operations:
- Individual snippet execution
- Snippet discovery and listing
- Argument parsing and validation
- Logging configuration

The module serves as the entry point for the 'pype snippets' command.

Example:
    pype snippets reverse_fa -i input.fa -o reversed.fa
    pype snippets list
    pype snippets --help
"""

from argparse import ArgumentParser, _SubParsersAction

from pype.modules.profiles import get_profiles
from pype.logger import PypeLogger
from pype.utils.snippets import snippets_modules_list
from pype.__config__ import PYPE_LOGDIR

PYPE_SNIPPETS_MODULES = snippets_modules_list({})

def add_parser(parser: _SubParsersAction, 
              module_name: str) -> ArgumentParser:
    """Add snippet command parser.
    
    Args:
        parser: Parent parser's subparsers
        module_name: Name of this module
        
    Returns:
        Parser for snippet commands
    """
    return parser.add_parser(
        module_name, 
        help="Execute individual snippet tasks",
        description="Run individual bioinformatics tasks as snippets",
        add_help=False
    )

def snippets_args(parser, argv, profile):
    lastparsers = parser.add_subparsers(dest="snippet")
    parser.add_argument(
        "--log",
        dest="log",
        type=str,
        default=PYPE_LOGDIR,
        help=("Path used for the snippet logs. Default: %s" % PYPE_LOGDIR),
    )
    for snippet_name in PYPE_SNIPPETS_MODULES:
        PYPE_SNIPPETS_MODULES[snippet_name].add_parser(lastparsers)
    args, extra = parser.parse_known_args(argv)
    try:
        used_snippet = args.snippet
        if used_snippet in PYPE_SNIPPETS_MODULES.keys():
            profile = get_profiles({})[profile]
            log = PypeLogger(used_snippet, args.log, profile)
            log.log.info("Prepare snippet %s" % used_snippet)
            log.log.info("Attempt to execute snippet %s" % used_snippet)
            PYPE_SNIPPETS_MODULES[used_snippet].snippet(
                lastparsers, extra, profile, log
            )
            log.log.info("Snippet %s executed" % used_snippet)
            return None
        if args.snippet is None:
            return parser.print_help()
        return parser.parse_args(args)
    except IndexError:
        return parser.print_help()


def snippets(parser, module_name, argv, profile):
    args = snippets_args(add_parser(parser, module_name), argv, profile)
    return args
