"""Pipeline command-line interface module.

This module provides the command-line interface for pipeline operations:
- Pipeline listing and discovery
- Pipeline execution and monitoring 
- Queue system integration
- Logging configuration

The module serves as the entry point for the 'pype pipelines' command.

Example:
    pype pipelines rev_compl_low_fa --input_fa input.fasta
    pype pipelines --queue slurm complex_pipeline --batch_file samples.txt
"""

from typing import Any, Optional, List
from argparse import ArgumentParser, _SubParsersAction

from pype.misc import get_modules_names, get_module_method, generate_uid
from pype.modules.profiles import get_profiles
from pype.utils.pipeline import get_pipelines, get_visible_pipelines
from pype.logger import PypeLogger
from pype.__config__ import PYPE_QUEUES, PYPE_LOGDIR


def add_parser(subparsers: _SubParsersAction, module_name: str) -> ArgumentParser:
    """Add pipeline command parser.
    
    Args:
        subparsers: Parent parser's subparsers
        module_name: Name of this module
        
    Returns:
        Parser for pipeline commands
    """
    return subparsers.add_parser(
        module_name,
        help="Execute pipeline workflows",
        description="Run bioinformatics pipelines with queue system integration",
        add_help=False
    )


def pipelines_args(parser: ArgumentParser, 
                  argv: List[str], 
                  profile: str) -> Optional[Any]:
    """Process pipeline command arguments.
    
    Args:
        parser: Command parser
        argv: Command line arguments
        profile: Profile name to use
        
    Returns:
        None if pipeline executed successfully, parser namespace otherwise
        
    Raises:
        Exception: If no queue systems are available
    """
    queues = get_modules_names(PYPE_QUEUES)
    if len(queues) < 1:
        raise Exception(
            ("There are no queues in %s " "to run a pipeline!!")
            % PYPE_QUEUES.__path__[0]
        )
    try:
        default_q = PYPE_QUEUES.default
    except AttributeError:
        default_q = queues[0]
    metavar_str = "{%s}" % ",".join(get_visible_pipelines())
    lastparsers = parser.add_subparsers(dest="pipeline", metavar=metavar_str)

    parser.add_argument(
        "--queue",
        dest="queue",
        action="store",
        choices=queues,
        default=default_q,
        help=(
            "Select the queuing system "
            "to run the pipeline. "
            "Default %s" % default_q
        ),
    )
    parser.add_argument(
        "--log",
        dest="log",
        type=str,
        default=PYPE_LOGDIR,
        help=("Path used to write the pipeline logs. " "Default %s" % PYPE_LOGDIR),
    )
    pipelines_list = get_pipelines(lastparsers, {})
    args, extra = parser.parse_known_args(argv)
    try:
        used_pipeline = args.pipeline
        if used_pipeline in pipelines_list.keys():
            profile_dict = get_profiles({})
            try:
                profile = profile_dict[profile]
            except KeyError:
                profile = profile_dict[profile_dict.keys()[0]]
            log = PypeLogger(
                "%s_%s" % (generate_uid(), used_pipeline), args.log, profile
            )
            pipelines_list[used_pipeline].submit(
                lastparsers, extra, args.queue, profile.__name__, log
            )
            log.log.info("Pipeline %s terminated" % log.__name__)
            log.log.info("Execute post run processes of queue %s, if any" % args.queue)
            post_run = get_module_method(PYPE_QUEUES, args.queue, "post_run")
            if post_run is not None:
                post_run(log)
            return None
        try:
            if args.pipeline is None:
                return parser.print_help()
            return parser.parse_args(args)
        except AttributeError:
            return parser.print_help()
    except IndexError:
        return parser.print_help()


def pipelines(subparsers, module_name, argv, profile):
    args = pipelines_args(add_parser(subparsers, module_name), argv, profile)
    return args
