"""Profile command-line interface module.

This module provides the command-line interface for profile management:
- Profile validation and checking
- Software/tool version verification  
- Resource path validation
- Profile information display

The module serves as the entry point for the 'pype profiles' command.

Example:
    pype profiles info
    pype profiles check my_profile
    pype profiles check my_profile --files
"""

from argparse import ArgumentParser, _SubParsersAction

from pype.utils.profiles import (
    get_profiles,
    check_profile_files,
    check_programs_path2, 
    print_checks
)

def add_parser(subparsers: _SubParsersAction, 
              module_name: str) -> ArgumentParser:
    """Add profile command parser.
    
    Args:
        subparsers: Parent parser's subparsers
        module_name: Name of this module
        
    Returns:
        Parser for profile commands
    """
    return subparsers.add_parser(
        module_name,
        help="Manage execution profiles",
        description="Configure and validate execution environments",
        add_help=False
    )


def profiles_args(parser, argv):
    lastparsers = parser.add_subparsers(dest="profile")
    lastparsers.add_parser(
        "info", add_help=False, help="Retrieve details from available profiles"
    )
    lastparsers.add_parser("check", add_help=False, help="Check if a profile is valid")
    args, extra = parser.parse_known_args(argv)
    profiles_list = get_profiles({})
    if args.profile == "info":
        info_profiles(lastparsers, profiles_list, extra)
    elif args.profile == "check":
        check_profiles(lastparsers, profiles_list, extra)
    else:
        parser.print_help()


def profiles(subparsers, module_name, argv, profile):
    profiles_args(add_parser(subparsers, module_name), argv)


def check_profiles(parser, profiles_list, argv):
    subparser = parser.add_parser("check", add_help=False)
    subparser.add_argument(
        "name",
        action="store",
        choices=profiles_list.keys(),
        help="Name of profile to check",
    )
    subparser.add_argument(
        "-f",
        "--files",
        dest="files",
        action="store_true",
        help="Check only the Profile files",
    )
    subparser.add_argument(
        "-p",
        "--programs",
        dest="programs",
        action="store_true",
        help="Check only the Profile programs",
    )
    args = subparser.parse_args(argv)
    profile = profiles_list[args.name]
    if args.files == args.programs:
        files = True
        programs = True
    else:
        files = args.files
        programs = args.programs
    if files is True:
        files_check = check_profile_files(profiles_list[args.name])
        print_checks(files_check, profile.files)
    if programs is True:
        programs_check = check_programs_path2(profile)
        print_checks(programs_check, profile.programs)


def info_profiles(parser, profiles_list, argv):
    subparser = parser.add_parser("info", add_help=False)
    subparser.add_argument(
        "-p",
        "--profile",
        dest="profile",
        action="store",
        choices=profiles_list.keys(),
        help="Print details of a selected profile",
    )
    subparser.add_argument(
        "-a", "--all", dest="all", action="store_true", help="List all profiles"
    )
    args = subparser.parse_known_args(argv)[0]
    if args.all:
        for profile in profiles_list:
            print("%s:\t%s" % (profile, profiles_list[profile].info["description"]))
    elif args.profile:
        used_profile = args.profile
        if used_profile in profiles_list.keys():
            profiles_list[used_profile].describe()
    else:
        subparser.print_help()
