import os
from pype.misc import try_import


def load_config(config_file):
    res = {}
    if os.path.isfile(config_file):
        with open(config_file, "rt") as config:
            for line in config:
                if line.startswith("#"):
                    continue
                key, val = line.split("=")
                res[key.strip()] = val.strip()
    return res


home_dir = os.path.expanduser("~")
PYPE_HOME = os.path.join(home_dir, ".bio_pype")
pype_config = os.path.join(PYPE_HOME, "config")

try:
    conf_dict = load_config(pype_config)
except Exception:
    conf_dict = {}

env_vars = [
    "PYPE_MODULES",
    "PYPE_SNIPPETS",
    "PYPE_PROFILES",
    "PYPE_PIPELINES",
    "PYPE_QUEUES",
    "PYPE_REPOS",
    "PYPE_NCPU",
    "PYPE_MEM",
    "PYPE_TMP",
    "PYPE_LOGDIR",
    "PYPE_DOCKER",
    "PYPE_SINGULARITY_CACHE",
]
env_dict = {}

for env_var in env_vars:
    env_dict[env_var] = os.environ.get(env_var)
    if env_dict[env_var] is None and env_var in conf_dict.keys():
        env_dict[env_var] = conf_dict[env_var]


PYPE_SNIPPETS = None
PYPE_PROFILES = None
PYPE_PIPELINES = None
PYPE_QUEUES = None
PYPE_REPOS = None


def get_default_base():
    base = os.path.dirname(os.path.abspath(__file__))
    return base


DEFAULT_PYPE_MODULES_PATH = os.path.join(get_default_base(), "pype_modules")


def import_or_default(module_path, module_name, default_base):
    if module_path:
        base, name = os.path.split(os.path.abspath(module_path))
        mod = try_import(base, name)
    else:
        mod = try_import(default_base, module_name)
    return mod


if env_dict["PYPE_MODULES"]:
    pype_snippets = try_import(env_dict["PYPE_MODULES"], "snippets")
    pype_profiles = try_import(env_dict["PYPE_MODULES"], "profiles")
    pype_pipelines = try_import(env_dict["PYPE_MODULES"], "pipelines")
    pype_queues = try_import(env_dict["PYPE_MODULES"], "queues")
    PYPE_SNIPPETS = pype_snippets
    PYPE_PROFILES = pype_profiles
    PYPE_PIPELINES = pype_pipelines
    PYPE_QUEUES = pype_queues
else:
    PYPE_SNIPPETS = import_or_default(
        env_dict["PYPE_SNIPPETS"], "snippets", DEFAULT_PYPE_MODULES_PATH
    )
    PYPE_PROFILES = import_or_default(
        env_dict["PYPE_PROFILES"], "profiles", DEFAULT_PYPE_MODULES_PATH
    )
    PYPE_PIPELINES = import_or_default(
        env_dict["PYPE_PIPELINES"], "pipelines", DEFAULT_PYPE_MODULES_PATH
    )
    PYPE_QUEUES = import_or_default(
        env_dict["PYPE_QUEUES"], "queues", DEFAULT_PYPE_MODULES_PATH
    )

if env_dict["PYPE_TMP"] is None:
    PYPE_TMP = "/tmp"
else:
    PYPE_TMP = env_dict["PYPE_TMP"]

if env_dict["PYPE_LOGDIR"] is None:
    PYPE_LOGDIR = os.path.join(PYPE_HOME, "logs")
else:
    PYPE_LOGDIR = env_dict["PYPE_LOGDIR"]


if env_dict["PYPE_REPOS"] is None:
    PYPE_REPOS = os.path.join(DEFAULT_PYPE_MODULES_PATH, "repos.yaml")
else:
    PYPE_REPOS = env_dict["PYPE_REPOS"]
    if not os.path.isfile(PYPE_REPOS):
        raise Exception("File %s does not exits" % PYPE_REPOS)

if env_dict["PYPE_DOCKER"] is None:
    PYPE_DOCKER = "docker"
else:
    PYPE_DOCKER = env_dict["PYPE_DOCKER"]

if env_dict["PYPE_SINGULARITY_CACHE"] is None:
    PYPE_SINGULARITY_CACHE = os.getcwd()
else:
    PYPE_SINGULARITY_CACHE = env_dict["PYPE_SINGULARITY_CACHE"]

PYPE_MEM = env_dict["PYPE_MEM"]
PYPE_NCPU = env_dict["PYPE_NCPU"]
