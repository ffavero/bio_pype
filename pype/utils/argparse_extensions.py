from argparse import ArgumentParser, _SubParsersAction
from typing import Optional, Dict, Any


class NestedArgumentParser(ArgumentParser):
    """ArgumentParser extension that better handles nested subcommands."""

    def __init__(self, *args, **kwargs):
        self.subcommand_groups: Dict[str, "_SubParsersAction"] = {}
        super().__init__(*args, **kwargs)

    def add_subcommand_group(self, name: str, **kwargs) -> "_SubParsersAction":
        """Add a new subcommand group (like 'snippets' or 'pipelines')."""
        if name not in self.subcommand_groups:
            self.subcommand_groups[name] = self.add_subparsers(
                title=name, dest=f"{name}_command", **kwargs
            )
        return self.subcommand_groups[name]

    def get_subcommand_group(self, name: str) -> Optional["_SubParsersAction"]:
        """Get an existing subcommand group."""
        return self.subcommand_groups.get(name)


class CommandGroup:
    """Base class for command groups like snippets or pipelines."""

    def __init__(self, name: str, description: str):
        self.name = name
        self.description = description
        self.subcommands: Dict[str, Any] = {}

    def add_to_parser(self, parser: NestedArgumentParser) -> None:
        """Add this command group to the parser."""
        group = parser.add_subcommand_group(
            self.name, help=self.description, description=self.description
        )

        for cmd_name, cmd in self.subcommands.items():
            cmd_parser = group.add_parser(cmd_name, help=cmd.description)
            cmd.setup_parser(cmd_parser)

    def register_command(self, name: str, command: Any) -> None:
        """Register a new subcommand."""
        self.subcommands[name] = command
