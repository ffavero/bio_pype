"""Profile management system for bio_pype.

Profiles define the execution environment and resource configuration:
- Software installation paths and versions
- Reference data locations
- Environment module configurations
- System resources (memory, CPU, etc)

Key functions:
    check_profile_files: Validate all file paths
    check_programs_env_module: Verify environment modules
    check_program: Validate program installation
    get_profiles: Load available profile configurations
"""

import os
import yaml
from pathlib import Path
from typing import Dict, Any, Optional, Union
from dataclasses import dataclass

from pype.misc import package_files
from pype.process import module_avail, program_string 
from pype.__config__ import PYPE_PROFILES
from pype.exceptions import ProfileError

@dataclass
class ProfileInfo:
    """Profile metadata."""
    description: str
    date: str
    version: str = "1.0.0"

@dataclass 
class ProfileProgram:
    """Software program configuration."""
    namespace: str
    path: str
    version: str
    modulepath: Optional[str] = None

def check_profile_files(profile: 'Profile') -> Dict[str, bool]:
    """Validate all file paths in profile.
    
    Args:
        profile: Profile to validate
        
    Returns:
        Dict mapping file keys to validation status
    """
    check_dict = {}
    for key in profile.files:
        resource = profile.files[key]
        if os.path.isfile(resource) or os.path.isdir(resource):
            check_dict[key] = True
        else:
            check_dict[key] = False
    return check_dict


def check_programs_env_module(profile: 'Profile') -> Dict[str, bool]:
    """Verify environment modules in profile.
    
    Args:
        profile: Profile to validate
        
    Returns:
        Dict mapping program keys to validation status
    """
    check_dict = {}
    for key in profile.programs:
        resource = profile.programs[key]
        mod_string = program_string(resource)
        try:
            modulepath = resource.modulepath
        except KeyError:
            modulepath = None
        res_avail = module_avail(resource.path, modulepath)
        check_dict[key] = False
        for line in res_avail:
            if mod_string in line:
                check_dict[key] = True
    return check_dict


def check_programs_path2(profile: 'Profile') -> Dict[str, bool]:
    """Validate program paths in profile.
    
    Args:
        profile: Profile to validate
        
    Returns:
        Dict mapping program keys to validation status
    """
    check_dict = {}
    for key in profile.programs:
        resource = profile.programs[key]
        namespace, program = resource.namespace.split("@")
        version = resource.version
        if namespace == "env_module":
            try:
                modulepath = resource.modulepath
            except KeyError:
                modulepath = None
            res_avail = module_avail(program, modulepath)
            check_dict[key] = False
            for line in res_avail:
                if "%s/%s" % (program, version) in line:
                    check_dict[key] = True
        else:
            res = check_program(program)
            check_dict[key] = res["is_file"]
    return check_dict


def check_program(path: str) -> Dict[str, bool]:
    """Check if a program is installed and executable.
    
    Args:
        path: Program path
        
    Returns:
        Dict with keys 'is_file' and 'is_exec' indicating status
    """
    is_file = False
    is_exec = False
    file_path = os.path.dirname(path)
    if file_path:
        if os.path.isfile(path):
            is_file = True
            if os.access(path, os.X_OK):
                is_exec = True
    else:
        for basedir in os.environ["PATH"].split(os.pathsep):
            basedir = basedir.strip('"')
            exe_file = os.path.join(basedir, path)
            if os.path.isfile(exe_file):
                is_file = True
                if os.access(exe_file, os.X_OK):
                    is_exec = True
    return {"is_file": is_file, "is_exec": is_exec}


def print_checks(check: Dict[str, bool], profile_dict: Dict[str, Any]) -> None:
    """Print the results of profile checks.
    
    Args:
        check: Dict of check results
        profile_dict: Profile data
    """
    greencol = "\033[92m"
    redcol = "\033[91m"
    endcol = "\033[0m"
    failed = {}
    for key, value in check.items():
        try:
            path = profile_dict[key]["namespace"]
            type_check = "Program"
        except TypeError:
            path = profile_dict[key]
            type_check = "File"
        if value is True:
            print("%s%s OK%s - %s path: %s" % (greencol, type_check, endcol, key, path))
        else:
            failed[key] = {"path": path, "type": type_check}
    for key, value in failed.items():
        print(
            "%s%s ERROR%s - %s path: %s"
            % (redcol, value["type"], endcol, key, value["path"])
        )


def get_profiles(profs: Dict[str, 'Profile']) -> Dict[str, 'Profile']:
    """Load available profile configurations.
    
    Args:
        profs: Dict to store loaded profiles
        
    Returns:
        Dict of loaded profiles
    """
    profiles = package_files(PYPE_PROFILES, ".yaml")
    for profile in sorted(profiles):
        try:
            prof_name = os.path.basename(os.path.splitext(profile)[0])
            profs[prof_name] = Profile(profile, prof_name)
        except AttributeError:
            pass
    return profs


class Profile:
    """Profile configuration container.
    
    Attributes:
        files: Dict of file/directory paths
        programs: Dict of program configurations 
        info: Profile metadata
        build: Genome build info
    """
    
    def __init__(self, path: Union[str, Path], name: str):
        """Initialize profile from YAML file.
        
        Args:
            path: Path to profile YAML file
            name: Profile name
            
        Raises:
            ProfileError: If profile is invalid
        """
        self.__path__ = path
        self.__name__ = name
        self.files: Dict[str, str] = {}
        self.programs: Dict[str, ProfileProgram] = {}
        self.info: ProfileInfo
        
        try:
            with open(self.__path__, 'rb') as f:
                profile = yaml.safe_load(f)
                for key, value in profile.items():
                    setattr(self, key, value)
        except Exception as e:
            raise ProfileError(f"Failed to load profile {name}", 
                            profile_name=name) from e

    def describe(self) -> None:
        """Print human readable profile description."""
        print("Name       : %s" % self.__name__)
        print("Description: %s" % self.info["description"])
        print("Date       : %s" % self.info["date"])
        print("Files      :")
        for file in self.files:
            print("\tID: %s" % file)
            print("\t\tpath: %s" % self.files[file])
        print("Programs   :")
        for program in self.programs:
            print("\tID: %s" % program)
            print("\t\tnamespace: %s" % self.programs[program]["namespace"])
            print("\t\tversion: %s" % self.programs[program]["version"])
        print("File Path  : %s" % self.__path__)
