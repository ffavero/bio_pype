# BIO pype

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/512653995b054d759860a4fadbb4f4a0)](https://www.codacy.com/bb/ffavero/bio_pype/dashboard?utm_source=ffavero@bitbucket.org&utm_medium=referral&utm_content=ffavero/bio_pype&utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/512653995b054d759860a4fadbb4f4a0)](https://www.codacy.com/bb/ffavero/bio_pype/dashboard?utm_source=ffavero@bitbucket.org&utm_medium=referral&utm_content=ffavero/bio_pype&utm_campaign=Badge_Coverage)
[![DeepSource](https://deepsource.io/bb/ffavero/bio_pype.svg/?label=active+issues&show_trend=true)](https://deepsource.io/bb/ffavero/bio_pype/?ref=repository-badge)
[![DeepSource](https://deepsource.io/bb/ffavero/bio_pype.svg/?label=resolved+issues&show_trend=true)](https://deepsource.io/bb/ffavero/bio_pype/?ref=repository-badge)

## About

Bio_pype is a robust pipeline management system designed specifically for bioinformatics workflows. It enhances scalability and reproducibility while simplifying daily bioinformatics analyses through a modular and flexible architecture.

The framework consists of three core components that work together to create efficient and maintainable analysis pipelines:

### Snippets

Snippets are the fundamental building blocks of bio_pype pipelines. They act as intelligent wrappers around bioinformatics tools by:

- Managing command-line arguments and validation
- Handling input/output preprocessing
- Providing consistent interfaces for tool execution
- Enabling seamless integration into larger workflows

### Pipelines

Pipelines orchestrate multi-step analyses by combining snippets in a defined order. They are:

- Defined using clear and maintainable YAML configurations
- Capable of handling complex dependency chains
- Easy to modify and extend
- Version controlled for reproducibility

### Profiles

Profiles manage execution environments and dependencies, allowing you to:

- Switch between different tool versions
- Configure reference data locations
- Manage system-specific settings
- Maintain consistent environments across executions

The profile system ensures that pipelines remain portable while accessing the correct versions of tools and data.

For detailed documentation, visit our [Read the Docs](http://bio-pype.readthedocs.io) site.
