.. index:: Configuration

.. _configuration:

Configuration
=============

Configuration Overview
-------------------

Bio_pype uses a flexible configuration system that allows you to:
- Customize module locations
- Set system-specific parameters
- Define execution environments
- Manage resource limits

Module Paths
----------

By default, Bio_pype modules (snippets, pipelines, profiles, and queues) are installed
in Python's site-packages directory. However, you can customize these locations to:
- Make modules easier to edit and maintain
- Switch between different module sets (e.g., stable vs. development)
- Share modules across users or projects

Configuration Methods
------------------

1. Local Configuration File
^^^^^^^^^^^^^^^^^^^^^^^^^

The primary configuration file is located at ``~/.bio_pype/config``. Example::

    PYPE_TMP=/tmp
    PYPE_LOGDIR=/tmp/logs
    PYPE_SNIPPETS=~/bio_pype/snippets
    PYPE_PIPELINES=~/bio_pype/pipelines

2. Environment Variables
^^^^^^^^^^^^^^^^^^^^^

Environment variables override settings in the configuration file::

    export PYPE_SNIPPETS=/custom/path/snippets
    export PYPE_MEM="32GB"

Environment Configuration
----------------------

The current module path is set to::

    $ echo $PYPE_MODULES
    test/data

Configuration Variables
--------------------

.. list-table::
   :widths: 33 66
   :header-rows: 1

   * - Variable
     - Description
   * - PYPE_MODULES
     - Base path for all module types (overrides individual module paths)
   * - PYPE_SNIPPETS
     - Path to snippet modules
   * - PYPE_PROFILES
     - Path to profile configurations
   * - PYPE_PIPELINES
     - Path to pipeline definitions
   * - PYPE_QUEUES
     - Path to queue system adapters
   * - PYPE_REPOS
     - Path to repository configuration file
   * - PYPE_NCPU
     - Maximum CPUs for parallel local execution
   * - PYPE_MEM
     - Maximum memory for local execution
   * - PYPE_TMP
     - Temporary directory (available as %(pype_tmp)s)
   * - PYPE_LOGDIR
     - Log file directory (default: ~/.bio_pype/logs)
   * - PYPE_DOCKER
     - Container runtime path (docker/singularity/udocker)
   * - PYPE_SINGULARITY_CACHE
     - Singularity image cache directory
