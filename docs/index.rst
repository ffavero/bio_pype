BIO Pype
============

A lightweight python framework to organize bioinformatics
scripts and analyses.

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   about
   install
   configure
   extra

.. toctree::
   :maxdepth: 2
   :caption: Core Components

   snippets
   pipelines
   profiles

.. toctree::
   :maxdepth: 2
   :caption: Usage & Troubleshooting

   logs
   api

Indexes and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
