.. index:: Snippets

.. _snippets:

Snippets
========

A snippet is the basic execution unit of Bio_pype.
It can be written either as a Markdown file using
code chunks to run arbitrary code, or
as a Python module (see `Advanced Snippets in Python`_)


Basic Snippet Structure
-----------------------

A complete snippet example is shown in the :ref:`simple_snippet` section.

A snippet consists of the following section headers:

   1. *requirements*: Contains a code chunk returning a dictionary
      that specifies the necessary resources to run the snippet
      (e.g., used to allocate resources in queuing systems)
   2. *results*: Contains a code chunk returning a dictionary
      listing all files produced by the snippet's execution
   3. *arguments*: A numbered list interpreted by :py:mod:`argparse`
      to produce the snippet's command line interface
   4. *snippet*: Contains the code chunks with instructions for
      performing the desired task
   5. *name*: An optional section containing a chunk that returns
      a "friendly name". This name overrides the default snippet
      name and helps identify log folders and job IDs more easily.

The input and output arguments are passed to the various chunk via
`variable substitutions by name`, a method used in
`python strings formatting`_.

In practice it means that a string `%(hello)s` present in a chunk,
would be replace
by the value of the variable `hello`

There are few ways of setting variables:

   1. The *arguments* section
   2. The `profiles.files` (See :ref:`profiles`)
   3. The keys from the *results* object

The arguments variables are named after the argument name, and the value is
the value passed to the the command line.

The variables from the `profile` and from the *results* section are
prefixed with `profile_` and `results_` respectively.
This means that in order to pass a key, eg. `genome_fa`, present in the
`profile.file`, in the snippet chunk it corresponds to
`%(profile_genome_fa)s`.

More detail on the argument passing in the following section

Reference arguments results and files
-------------------------------------


Using Namespaces
----------------


<<This section may go to :ref:`profiles`>>
The `namespace` are set in the `profile` file. Ideally the snippet
should be agnostic on the final runtime execution, and it may be
possible to run it as-is in different environment by only change the
namespace in the profile.

More broadly the `namespace` is a mechanism to set the environment to
where execute the chunk.

Supported namespace are:

   1. Path: assumes that the commands in the chunks are present in the
      environment `$PATH`
   2. `Environment Modules`_: loads a set of specified modules before
      running the chunk
   3. Docker: run the chunk within a container image. This `namespace`
      supports also uDocker and singularity


Path
""""


Environment Modules
"""""""""""""""""""


Docker/Singularity/uDocker
""""""""""""""""""""""""""



Advanced Snippets in Python
---------------------------

The snippets are located in a python module (mind the __init__.py
in the folder containing the snippets).
In order to function, each snippet need to have 4 specific function:

    1. *requirements*: a function returning a dictionary with the
       necessary resource to run the snippet (used to allocate
       resource in queuing systems)
    2. *results*: a function accepting a dictionary with the snippet
       arguments and returning a dictionary listing all the files
       produced by the execution of the snippet
    3. *add_parser*: a function that implement the
       :py:mod:`argparse` module and defines the command line
       arguments accepted by the snippet
    4. a function named as the snippet file name (without
       the *.py* extension), containing the code for the execution of
       the tool


.. literalinclude:: ../test/data/snippets/test_adv.py
   :language: python


.. _python strings formatting: https://docs.python.org/3/library/stdtypes.html#printf-style-string-formatting
.. _Environment Modules: http://modules.sourceforge.net
