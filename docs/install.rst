.. index:: Get started

Getting Started
=============

Requirements
-----------

Bio_pype requires:

- Python 3.4 or later
- PyYAML for configuration parsing
- psutil for resource management
- semantic-version for version handling

.. note::
   While earlier versions supported Python 2.7, Bio_pype now requires Python 3
   following Python 2's end-of-life in January 2020.

Installation
-----------

Using Virtual Environments
^^^^^^^^^^^^^^^^^^^^^^
We strongly recommend using virtual environments::

    python -m venv bio_pype_env
    source bio_pype_env/bin/activate

From PyPI
^^^^^^^^
For most users, installing from PyPI is recommended::

    pip install bio_pype

From Source
^^^^^^^^^
For latest features or development::

    git clone https://bitbucket.org/ffavero/bio_pype
    cd bio_pype
    python -m unittest discover
    pip install .

Basic Setup
---------

1. Create configuration directory::

    mkdir -p ~/.bio_pype

2. Create minimal configuration::

    cat > ~/.bio_pype/config << EOF
    PYPE_SNIPPETS=~/bio_pype/snippets
    PYPE_PIPELINES=~/bio_pype/pipelines
    PYPE_PROFILES=~/bio_pype/profiles
    EOF

3. Create module directories::

    mkdir -p ~/bio_pype/{snippets,pipelines,profiles}

First Steps
---------

Try your first snippet (see :ref:`simple_snippet`):

1. Create a test snippet
2. Configure your environment
3. Run the snippet
4. Verify the output

For detailed configuration, see :ref:`configuration`.
For profile setup, see :ref:`profiles`.

.. _virtualenv: https://virtualenv.pypa.io
.. _python package index: https://pypi.org
