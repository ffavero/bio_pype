# Building Documentation

1. Create a docs environment:
```bash
python -m venv docs_env
source docs_env/bin/activate
```

2. Install requirements:
```bash
pip install -r requirements.txt
```

3. Build docs:
```bash
make html
```

# Troubleshooting

If you see command execution errors, ensure bio_pype is installed:
```bash
pip install -e ..
```
