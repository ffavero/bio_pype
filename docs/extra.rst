.. index:: Available modules

Available modules:
==================

Browse and install available modules
____________________________________

There are available set of snippets and pipelines. You can access the
list of repository and manage the installed modules with the `repos`
sub command


.. command-output:: pype repos
  :returncode: 0


The currently available sets are:

.. command-output:: pype repos list
  :returncode: 0


It's possible to install the modules from a repository
in the list, by  invoking the `pype repos install` with
the selected repository.
