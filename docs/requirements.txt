sphinx>=4.0
sphinx-rtd-theme>=1.0
sphinxcontrib-programoutput
myst-parser>=1.0.0
PyYAML
psutil
argparse==1.4.0
semantic-version>=2.10.0
