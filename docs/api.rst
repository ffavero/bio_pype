API Documentation
===============

.. currentmodule:: pype

.. autosummary::
   :toctree: _autosummary
   :recursive:
   :template: custom-module-template.rst

   pype
   pype.utils
   pype.process
   pype.misc
   pype.logger
   pype.exceptions

Core Components
-------------

Pipeline Management
^^^^^^^^^^^^^^^^^

.. currentmodule:: pype.utils.pipeline
.. autosummary::
   :toctree: _autosummary
   :nosignatures:

   Pipeline
   PipelineItem
   get_pipelines
   get_visible_pipelines

Snippet Management
^^^^^^^^^^^^^^^

.. currentmodule:: pype.utils.snippets
.. autosummary::
   :nosignatures:

   Snippet
   SnippetArguments
   snippets_modules_list

Queue Management
^^^^^^^^^^^^^

.. currentmodule:: pype.utils.queues
.. autosummary::
   :nosignatures:

   QueueJob
   SnippetRuntime

Profiles
^^^^^^^

.. currentmodule:: pype.utils.profiles
.. autosummary::
   :nosignatures:

   Profile
   check_profile_files
   check_programs_env_module

Utilities
--------

Process Management
^^^^^^^^^^^^^^^

.. currentmodule:: pype.process
.. autosummary::
   :nosignatures:

   Command
   Namespace
   DockerConfig
   Volume

Error Handling
^^^^^^^^^^^^

.. currentmodule:: pype.exceptions
.. autosummary::
   :nosignatures:

   PypeError
   PipelineError
   SnippetError
   ProfileError
   CommandError
