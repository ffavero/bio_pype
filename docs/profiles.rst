.. index:: Profiles

.. _profiles:

Profiles
========

Profile Configuration
------------------

Profiles define execution environments using YAML files. Like pipelines,
profiles must be placed in a Python module (directory with __init__.py).

Profile Structure
--------------

A profile requires these main sections:

1. info
    - description: Profile purpose
    - date: Creation/update date
    - version: Profile version

2. files
    - Reference data paths
    - Input/output directories
    - Resource locations

3. programs
    - Software configurations
    - Environment modules
    - Container images

Example Profiles
-------------

Basic Path-based Profile:

.. literalinclude:: ../test/data/profiles/test_path.yaml
   :language: yaml
   :caption: Path-based profile example
   :name: path-profile

Container-based Profile:

.. literalinclude:: ../test/data/profiles/test_docker.yaml
   :language: yaml
   :caption: Docker profile example
   :name: docker-profile
